<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 10.01.19
 * Time: 17:15
 */
class Country extends Model {
    protected $table = 'country';
}