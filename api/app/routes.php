<?php
// Routes
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/', App\Action\HomeAction::class)
    ->setName('homepage');

$app->map(['GET', 'POST'], '/api/country[/{term}[/{limit}]]', function ($request, $response, $args) {

    $term = isset($args['term']) ? $args['term'] : "";
    $limit = isset($args['limit']) ? $args['limit'] : "";

    $name = "%$term%";

    $where = '';
    $limits = '';

    if ($term) {
        $where = " where name LIKE :name ";
    }

    if ($limit) {
        $limits = " LIMIT :limit ";
    }

    $selectStatement = $this->pdo->prepare("select id, name from country $where GROUP BY name ORDER BY id ASC $limits");

    $selectStatement->bindValue(':name', $name, PDO::PARAM_STR);
    $selectStatement->bindParam(':limit', $limit, PDO::PARAM_INT);

    $selectStatement->execute();

    $records = $selectStatement->fetchAll(PDO::FETCH_ASSOC);

    return $this->response->withJson($records);
});

$app->map(['GET', 'POST'], '/api/city/{country}[/{term}[/{limit}]]', function ($request, $response, $args) {

    $country = isset($args['country']) ? $args['country'] : "";
    $term = isset($args['term']) ? $args['term'] : "";
    $limit = isset($args['limit']) ? $args['limit'] : 100;

    $selectStatement = $this->pdo->prepare("select id as countryId from country where pid = :pid ");

    $selectStatement->bindValue(':pid', $country, PDO::PARAM_STR);

    $selectStatement->execute();

    $record = $selectStatement->fetchAll(PDO::FETCH_ASSOC);

    if (isset($record[0]['countryId'])) {

        $countryId = $record[0]['countryId'];

        $name = "%$term%";

        $where = '';
        $limits = '';

        if ($term) {
            $where = " AND name LIKE :name ";
        }

        if ($limit) {
            $limits = " LIMIT :limit ";
        }

        $selectStatement = $this->pdo->prepare("select id, name from city where country_id = :country_id $where GROUP BY name ORDER BY id ASC $limits");

        $selectStatement->bindParam(':country_id', $countryId, PDO::PARAM_INT);
        $selectStatement->bindValue(':name', $name, PDO::PARAM_STR);
        $selectStatement->bindParam(':limit', $limit, PDO::PARAM_INT);
        $selectStatement->execute();

        $records = $selectStatement->fetchAll(PDO::FETCH_ASSOC);

        return $this->response->withJson($records);
    }

    return $this->response->withJson([]);
});